const int henlo = 5;
//const int ignored = "fren";

instance test(c_info) {
	x = 5 + 2 + henlo + 5 + 3;
	y = 6 + 9 + unknown + 3;
	z = "henlo fren";
	w = henlo;
	//ignored = ???;
};

const /*ignored*/ int unknown = 10 /* also // ignored */ / 3;
const string contains_a_comment = "y r u // geh";

instance test2(c_info) {};
