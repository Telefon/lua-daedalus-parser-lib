local parser = {};

-- Only parsing the full src file is sane because definitions in files may depend on definitions from earlier loaded files.
-- Maybe it could be useful to save the parsed data and return a part of it when using parse functions for files or directories.

-- Debug functions
local function DebugGetFuncName(func)
	for k, v in pairs(getfenv()) do
		if v == func then
			return k;
		end
	end
end

local function DebugPrintFunction(func)
	local info = debug.getinfo(func, "S");
	print(string.format("function %s(), %s: %s", DebugGetFuncName(func) or "<anonymous>", info.short_src, info.linedefined));
end

function DeepPrint(value, indentLevel)
	if type(value) ~= "table" and type(value) ~= "function" then
		print(value);
		return;
	end

	if type(value) == "function" then
		DebugPrintFunction(value);
		return;
	end

	indentLevel = indentLevel or 0;
	local indentStr = string.rep('\t', indentLevel);
	print("{");
	for k, v in pairs(value) do
		local innerIndentStr = string.rep('\t', indentLevel + 1);
		io.write(innerIndentStr .. k .. " = ");
		if type(v) == "table" then
			DeepPrint(v, indentLevel + 1);
		elseif type(v) == "function" then
			DebugPrintFunction(v);
		else
			print(v);
		end
	end
	print(indentStr .. "}");
end

local data = {};

---@param src_filepath string
---@return table data
function ParseWithSrc(src_filepath)
	return data;
end

---@param filepath string
---@param src_filepath string
---@return table data
function ParseFile(filepath, src_filepath)
	-- scanner.init(filepath);
	-- print(scanner.state.text);
	-- scanner.debug_print_tokens();
	parser.parse_file(filepath);

	return data;
end


--
-- helper functions
--

local iota_value = 0;

---@param value integer|nil
---@return integer
local function iota(value)
	if value then
		iota_value = value;
	end
	local result = iota_value;
	iota_value = iota_value + 1;
	return result;
end

local scanner = {};
local error = {};
local ast = {}; -- Abstract syntax representation.


--
-- scanner implementation
--

---@enum token_type
scanner.token_type =
{
	uninitialized = iota(0),
	invalid       = iota(),

	-- symbols
	left_paren    = iota(),
	right_paren   = iota(),
	left_curly    = iota(),
	right_curly   = iota(),
	left_brack    = iota(),
	right_brack   = iota(),
	equals        = iota(),
	semicolon     = iota(),

	-- arithmetic operators
	plus          = iota(),
	minus         = iota(),
	mul           = iota(),
	div           = iota(),

	-- keywords
	keyword_begin = iota(),
	INSTANCE      = iota(),
	CONST         = iota(),
	keyword_end   = iota(),

	-- other
	ident         = iota(),
	string        = iota(),
	integer       = iota(),

	eof           = iota(),
	count         = iota(),
};

---@alias SourceLocation {filepath: string, start_pos: integer, end_pos: integer}

---@alias Token {type: token_type, value: string|number|nil, loc: SourceLocation}

---@alias ScannerState {text: string, filepath: string, pos: integer, token: Token, lookahead: Token}
scanner.state = {};

---@param filepath string
function scanner.init(filepath)
	local f = io.open(filepath, "rb");
	if not f then
		return error.fatal("could not open input file", filepath, 0); -- Return fixes "missing" nil checks.
	end

	---@type Token
	local uninitialized_token =
	{
		type = scanner.token_type.uninitialized,
		value = nil,
		loc = {filepath = filepath, start_pos = 0, end_pos = 0}
	};

	scanner.state =
	{
		filepath = filepath,
		text = f:read("*a"), ---@type string Daedalus is case insensitive.
		pos = 0, ---@type integer? nil signals the end of the file
		token = uninitialized_token,
		lookahead = uninitialized_token,
	};
end

---Gets called by the parser after the last instance was parsed.
function scanner.skip_to_next_instance()
	scanner.state.pos = scanner.state.text:find("[Ii][Nn][Ss][Tt][Aa][Nn][Cc][Ee]", scanner.state.pos);
end

-- TODO: If there is other code than a constant or an instance, comments are not recognized properly
function scanner.skip_to_next_definition()
	scanner.skip_to_next_code();
	if not scanner.state.pos then
		return;
	end

	local const_position = scanner.state.text:find("[Cc][Oo][Nn][Ss][Tt]", scanner.state.pos);
	local instance_position = scanner.state.text:find("[Ii][Nn][Ss][Tt][Aa][Nn][Cc][Ee]", scanner.state.pos);
	if const_position then
		if instance_position then
			scanner.state.pos = math.min(const_position, instance_position);
		else
			scanner.state.pos = const_position;
		end
	else
		scanner.state.pos = instance_position;
	end
end

-- skips spaces and comments
function scanner.skip_to_next_code()
	local state = scanner.state;
	while state.pos do
		state.pos = state.text:find("%S", state.pos);
		if not state.pos then
			return;
		end

		local comment = state.text:sub(state.pos, state.pos + 1);
		if comment == "//" then
			state.pos = select(2, state.text:find("\n.", state.pos + 2));
		elseif comment == "/*" then
			state.pos = select(2, state.text:find("%*/.", state.pos + 2));
		else
			return;
		end
	end
end

function scanner.peek_token()
	local lookahead = scanner.state.lookahead;
	if lookahead.type == scanner.token_type.uninitialized then
		scanner.lex_token(lookahead);
	end
	return lookahead;
end

function scanner.next_token()
	local state = scanner.state;
	if state.lookahead.type ~= scanner.token_type.uninitialized then
		state.token.type = state.lookahead.type;
		state.token.value = state.lookahead.value;
		state.token.loc = state.lookahead.loc;
		-- TODO: why does this not work and we need a new table?
		-- state.lookahead.type = scanner.token_type.uninitialized;
		state.lookahead = {type = scanner.token_type.uninitialized, value = nil, loc = {}};
	else
		scanner.lex_token(state.token);
	end
	return state.token;
end

---@param token? Token
---@param loc SourceLocation
---@param token_type token_type
---@param length integer
function scanner.lex_symbol(token, loc, token_type, length)
	scanner.state.pos = loc.end_pos + length;
	token.type = token_type;
	token.loc = loc;
	return token;
end

---@param token? Token
---@return Token?
function scanner.lex_token(token)
	local state = scanner.state;

	scanner.skip_to_next_code();
	if not state.pos then
		return nil;
	end

	---@type SourceLocation
	local loc = {filepath = state.filepath, start_pos = state.pos, end_pos = state.pos};

	local ch = state.text:sub(state.pos, state.pos);
	if ch == "(" then
		return scanner.lex_symbol(token, loc, scanner.token_type.left_paren, 1);
	elseif ch == ")" then
		return scanner.lex_symbol(token, loc, scanner.token_type.right_paren, 1);
	elseif ch == "{" then
		return scanner.lex_symbol(token, loc, scanner.token_type.left_curly, 1);
	elseif ch == "}" then
		return scanner.lex_symbol(token, loc, scanner.token_type.right_curly, 1);
	elseif ch == "[" then
		return scanner.lex_symbol(token, loc, scanner.token_type.left_brack, 1);
	elseif ch == "]" then
		return scanner.lex_symbol(token, loc, scanner.token_type.right_brack, 1);
	elseif ch == "=" then
		return scanner.lex_symbol(token, loc, scanner.token_type.equals, 1);
	elseif ch == ";" then
		return scanner.lex_symbol(token, loc, scanner.token_type.semicolon, 1);
	elseif ch == "+" then
		return scanner.lex_symbol(token, loc, scanner.token_type.plus, 1);
	elseif ch == "-" then
		return scanner.lex_symbol(token, loc, scanner.token_type.minus, 1);
	elseif ch == "*" then
		return scanner.lex_symbol(token, loc, scanner.token_type.mul, 1);
	elseif ch == "/" then
		return scanner.lex_symbol(token, loc, scanner.token_type.div, 1);
	end

	-- if text then
	if ch:match("[%a_]") then
		local text = state.text:match("[%l_][%l%d_]*", state.pos):upper();
		loc.end_pos = state.pos + text:len() - 1;
		state.pos = loc.end_pos + 1;
		token.loc = loc;
		for keyword_name, value in pairs(scanner.token_type) do
			if  value > scanner.token_type.keyword_begin
			and value < scanner.token_type.keyword_end
			and keyword_name == text
			then
				token.type = value;
				return token;
			end
		end
		token.type = scanner.token_type.ident;
		token.value = text;
		return token;
	end

	if ch:match("%d") then
		local text = state.text:match("%d+", state.pos);
		loc.end_pos = state.pos + text:len() - 1;
		state.pos = loc.end_pos + 1;
		token.loc = loc;
		token.type = scanner.token_type.integer;
		token.value = tonumber(text);
		return token;
	end

	-- TODO: Handle escape characters.

	if ch == '"' then
		local text = state.text:match('".-"', state.pos);
		loc.end_pos = state.pos + text:len() - 1;
		state.pos = loc.end_pos + 1;
		token.loc = loc;
		token.type = scanner.token_type.string;
		token.value = text;
		return token;
	end

	token.type = scanner.token_type.invalid;
	return token;
end

---@param pos integer
---@return integer line, integer column
function scanner.get_line_and_column(pos)
	local text = scanner.state.text;
	local line = 1;
	local column = 0;

	-- skip whitespace
	for p = 1, pos do
		local ch = text:sub(p, p);
		if ch == "\n" then
			line = line + 1;
			column = 0;
		elseif ch == "\t" then
			column = column + 4;
		else
			column = column + 1;
		end
	end

	return line, column;
end

---@param filepath string
---@param pos integer
---@return string
function scanner.format_source_location(filepath, pos)
	local line, column = scanner.get_line_and_column(pos);
	return filepath .. ":" .. tostring(line) .. ":" .. tostring(column);
end

function scanner.token_tostring(token)
	if token.type == scanner.token_type.uninitialized then
		return "<uninitialized>";
	elseif token.type == scanner.token_type.invalid then
		return "invalid";
	elseif token.type == scanner.token_type.left_paren then
		return "'('";
	elseif token.type == scanner.token_type.right_paren then
		return "')'";
	elseif token.type == scanner.token_type.left_curly then
		return "'{'";
	elseif token.type == scanner.token_type.right_curly then
		return "'}'";
	elseif token.type == scanner.token_type.left_brack then
		return "'['";
	elseif token.type == scanner.token_type.right_brack then
		return "']'";
	elseif token.type == scanner.token_type.equals then
		return "'='";
	elseif token.type == scanner.token_type.semicolon then
		return "';'";
	elseif token.type == scanner.token_type.plus then
		return "'+'";
	elseif token.type == scanner.token_type.minus then
		return "'-'";
	elseif token.type == scanner.token_type.mul then
		return "'*'";
	elseif token.type == scanner.token_type.div then
		return "'/'";
	elseif token.type == scanner.token_type.INSTANCE then
		return "keyword(INSTANCE)";
	elseif token.type == scanner.token_type.CONST then
		return "keyword(CONST)";
	elseif token.type == scanner.token_type.ident then
		return "ident(" .. token.value .. ")";
	elseif token.type == scanner.token_type.string then
		return "string(\"" .. token.value .. "\")";
	elseif token.type == scanner.token_type.integer then
		return "integer(" .. tostring(token.value) .. ")";
	elseif token.type == scanner.token_type.eof then
		return "EOF";
	end

	return "<unexpected token>";
end

---@param type token_type
function scanner.token_type_tostring(type)
	if type == scanner.token_type.uninitialized then
		return "<uninitialized>";
	elseif type == scanner.token_type.invalid then
		return "invalid";
	elseif type == scanner.token_type.left_paren then
		return "'('";
	elseif type == scanner.token_type.right_paren then
		return "')'";
	elseif type == scanner.token_type.left_curly then
		return "'{'";
	elseif type == scanner.token_type.right_curly then
		return "'}'";
	elseif type == scanner.token_type.left_brack then
		return "'['";
	elseif type == scanner.token_type.right_brack then
		return "']'";
	elseif type == scanner.token_type.equals then
		return "'='";
	elseif type == scanner.token_type.semicolon then
		return "';'";
	elseif type == scanner.token_type.plus then
		return "'+'";
	elseif type == scanner.token_type.minus then
		return "'-'";
	elseif type == scanner.token_type.mul then
		return "'*'";
	elseif type == scanner.token_type.div then
		return "'/'";
	elseif type == scanner.token_type.INSTANCE then
		return "keyword(INSTANCE)";
	elseif type == scanner.token_type.CONST then
		return "keyword(CONST)";
	elseif type == scanner.token_type.ident then
		return "ident";
	elseif type == scanner.token_type.string then
		return "string";
	elseif type == scanner.token_type.integer then
		return "integer";
	elseif type == scanner.token_type.eof then
		return "EOF";
	end

	return "<unexpected token>";
end

---@param token Token
function scanner.print_token(token)
	local loc_str = scanner.format_source_location(token.loc.filepath, token.loc.start_pos);
	print(loc_str .. ": token = " .. scanner.token_tostring(token));
end

function scanner.debug_print_tokens()
	scanner.skip_to_next_definition();
	local token = scanner.next_token();
	local i = 0;
	while token and i < 100 do
		scanner.print_token(token)
		token = scanner.next_token();
		i = i + 1;
	end
end


--
-- Parser implementation
--

function parser.parse_file(filepath)
	scanner.init(filepath);
	print(scanner.state.text);
	print("\27[1;31m---------- END OF INPUT ----------\27[0m\n");

	scanner.skip_to_next_definition();
	local peek = scanner.peek_token();

	while peek.type == scanner.token_type.INSTANCE or peek.type == scanner.token_type.CONST do
		if peek.type == scanner.token_type.INSTANCE then
			local instance = parser.parse_instance();
			if instance then
				print(ast.show_instance(instance));
				local t = {class = instance.class_name};
				for k, v in pairs(instance.assignments) do
					DeepPrint(k);
					-- TODO: Handle arrays.
					t[k] = v.value;
				end
				data[instance.name] = t;
			end
		elseif peek.type == scanner.token_type.CONST then
			local const_definition = parser.parse_constant();
			if const_definition then
				print(ast.show_constant(const_definition));
				data[const_definition.name] = const_definition.value;
			end
		end
		scanner.skip_to_next_definition();
		peek = scanner.peek_token();
	end

	print("finished parsing instances and constants");
end

---Syntax: "const" <type-expression> <identifier> "=" <expression> ";"
---@return AST_Constant|nil
function parser.parse_constant()
	local token = scanner.next_token(); -- Eat CONST keyword.

	token = scanner.next_token(); -- Variable type, don't really care.
	if not parser.expect(scanner.token_type.ident, token) then
		return;
	end
	local type_expression = token.value;

	token = scanner.next_token();
	if not parser.expect(scanner.token_type.ident, token) then
		return;
	end
	local variable_name = token.value;

	token = scanner.next_token();
	if not parser.expect(scanner.token_type.equals, token) then
		return;
	end

	local value = parser.parse_expression(0);

	token = scanner.next_token();
	if not parser.expect(scanner.token_type.semicolon, token) then
		return;
	end

	return {name = variable_name, type_expression = type_expression, value = value};
end

---@return AST_Instance|nil
function parser.parse_instance()
	local token = scanner.next_token(); -- Eat INSTANCE keyword.

	token = scanner.next_token();
	if not parser.expect(scanner.token_type.ident, token) then
		return;
	end
	local instance_name = token.value;
	-- print("instance name: " .. instance_name);

	token = scanner.next_token();
	if not parser.expect(scanner.token_type.left_paren, token) then
		return;
	end

	token = scanner.next_token();
	if not parser.expect(scanner.token_type.ident, token) then
		return;
	end
	local class_name = token.value;
	-- print("class name: " .. class_name);

	token = scanner.next_token();
	if not parser.expect(scanner.token_type.right_paren, token) then
		return;
	end

	token = scanner.next_token();
	if not parser.expect(scanner.token_type.left_curly, token) then
		return;
	end

	local assignments = parser.parse_body();

	token = scanner.next_token();
	if not parser.expect(scanner.token_type.right_curly, token) then
		return;
	end

	token = scanner.next_token();
	if not parser.expect(scanner.token_type.semicolon, token) then
		return;
	end

	-- return instance_name, assignments;

	return {name = instance_name, class_name = class_name, assignments = assignments};
end

---Grammar for one definition in the body.
---<identifier> { '[' <integer> ']' } '=' <expression> ';'
---@return table<variable_type, expression_type>
function parser.parse_body()
	local result = {};

	-- TODO: If parsing of one definition fails, try the next after the next semicolon.
	while scanner.peek_token().type == scanner.token_type.ident do
		local token = scanner.next_token(); -- Eat identifier.
		local variable_name = token.value --[[@as string]];

		local variable = parser.parse_variable(variable_name);
		if not variable then
			break;
		end

		token = scanner.next_token();
		if not parser.expect(scanner.token_type.equals, token) then
			break;
		end

		local value = parser.parse_expression(0);

		token = scanner.next_token();
		if not parser.expect(scanner.token_type.semicolon, token) then
			break;
		end

		-- TODO: Think about array access.
		if variable.array then
			result[variable_name] = {value = variable};
			local iter = variable;
			while iter.array do
				iter = iter.array;
			end
		else
			result[variable_name] = value;
		end
		-- print("found: " .. variable_name .. " = " .. value .. ";");
	end

	return result;
end

---@param variable_name string
---@return AST_Variable|nil
function parser.parse_variable(variable_name)
	---@type AST_Variable
	local result = {type = ast.type.variable_type.ident, value = variable_name};

	local tmp = {};
	while scanner.peek_token().type == scanner.token_type.left_brack do
		local token = scanner.next_token(); -- Eat left bracket.
		local expression = parser.parse_expression(0);
		token = scanner.next_token();
		if not parser.expect(scanner.token_type.right_brack, token) then
			return nil;
		end
		tmp[#tmp+1] = expression;
	end

	-- AST_ArrayAccess {array: AST_Variable, expression: AST_Expression}

	for i = #tmp, 1, -1 do
		result = {type = ast.type.variable_type.array, value = {array = result, expression = tmp[i]}};
	end

	-- 	-- TODO: Wrong direction, right most is the top of the table nesting but should be the lowest.
	-- 	result = {array = result, expression = expression};
	return result;
end

-- Source for PRATT parsing: https://matklad.github.io/2020/04/13/simple-but-powerful-pratt-parsing.html

---@type table<token_type, {left: integer, right: integer}>
parser.infix_binding_power =
{
	[scanner.token_type.plus]  = {left = 1, right = 2},
	[scanner.token_type.minus] = {left = 1, right = 2},
	[scanner.token_type.mul]   = {left = 3, right = 4},
	[scanner.token_type.div]   = {left = 3, right = 4},
};

-- TODO: Parse arrays.
---@param min_power integer
---@return AST_Expression
function parser.parse_expression(min_power)
	local first = scanner.next_token();
	local lhs;
	if first.type == scanner.token_type.integer then
		lhs = {type = ast.type.expression_type.integer, value = first.value --[[@as integer]]};
	elseif first.type == scanner.token_type.ident then
		lhs = {type = ast.type.expression_type.variable, value = parser.parse_variable(first.value --[[@as string]])};
	elseif first.type == scanner.token_type.string then
		lhs = {type = ast.type.expression_type.string, value = first.value --[[@as string]]};
	end

	while true do
		local op = scanner.peek_token();
		if op.type == scanner.token_type.eof or op.type == scanner.token_type.semicolon then
			break;
		end

		local binding_power = parser.infix_binding_power[op.type];
		if not binding_power then
			error.parser_error("Unknown operator " .. scanner.token_tostring(op) .. ".", scanner.state.filepath, scanner.state.pos);
			return lhs;
		end

		if binding_power.left < min_power then
			break;
		end

		scanner.next_token(); -- Eat operator.
		local rhs = parser.parse_expression(binding_power.right);

		lhs = {type = ast.type.expression_type.binary, value = {operator = op.type, lhs = lhs, rhs = rhs}};
	end

	return ast.evaluate_expression(lhs);
end

function parser.expect(expected_type, token)
	if token == nil or token.type ~= expected_type then
		error.parser_error("Unexpected token " .. scanner.token_tostring(token) .. ", expected " .. scanner.token_type_tostring(expected_type) .. ".", token.loc.filepath, token.loc.start_pos);
		scanner.skip_to_next_definition(); -- skip this instance
		return false;
	end

	return true;
end


--
-- Expressions
--

ast.type = {};

---@enum variable_type
ast.type.variable_type =
{
	ident = iota(0),  -- <ident>
	array = iota(),   -- <ident> { '[' <expression> ']' }
};

---@enum expression_type
ast.type.expression_type =
{
	integer = iota(0),
	string = iota(),
	variable = iota(),
	binary = iota(),
};

---@type table<token_type, fun(lhs: integer, rhs: integer): AST_Expression>
local evaluation_functions =
{
	[scanner.token_type.plus]  = function(lhs, rhs) return {type = ast.type.expression_type.integer, value = lhs + rhs}; end,
	[scanner.token_type.minus] = function(lhs, rhs) return {type = ast.type.expression_type.integer, value = lhs - rhs}; end,
	[scanner.token_type.mul]   = function(lhs, rhs) return {type = ast.type.expression_type.integer, value = lhs * rhs}; end,
	[scanner.token_type.div]   = function(lhs, rhs) return {type = ast.type.expression_type.integer, value = lhs / rhs}; end,
};

---Evaluates an expression as far as it can. Most expressions should be able to be evaluated to a single value.
---@param expression AST_Expression
---@return AST_Expression
function ast.evaluate_expression(expression)
	if expression.type == ast.type.expression_type.integer
	or expression.type == ast.type.expression_type.string
	then
		return expression;
	end

	-- TODO: Arrays are not handled.
	if expression.type == ast.type.expression_type.variable then
		return data[expression.value.value --[[@as string]]] or expression;
	end

	-- For now: expression is a binary. May be expanded later.
	local lhs = ast.evaluate_expression(expression.value.lhs);
	local rhs = ast.evaluate_expression(expression.value.rhs);

	-- We can only evaluate further when both operands are integers.
	if lhs.type == ast.type.expression_type.integer and rhs.type == ast.type.expression_type.integer then
		return evaluation_functions[expression.value.operator](lhs.value --[[@as integer]], rhs.value --[[@as integer]]);
	end
	return {type = ast.type.expression_type.binary, value = {operator = expression.value.operator, lhs = lhs, rhs = rhs}};
end

---@param definition AST_Constant
---@return string
function ast.show_constant(definition)
	return "CONST " .. definition.type_expression .. " " .. definition.name .. " = " .. ast.show_expression(definition.value) .. ";";
end

---@param instance AST_Instance
---@return string
function ast.show_instance(instance)
	local result = "INSTANCE " .. instance.name .. "(" .. instance.class_name .. ")\n{\n";
	for k, v in pairs(instance.assignments) do
		if v.value then
			result = result .. "\t" .. k .. " = " .. ast.show_expression(v) .. ";\n";
		end
	end
	return result .. "};"
end

---@type table<token_type, string>
ast.show_binary_operator =
{
	[scanner.token_type.plus]  = "+",
	[scanner.token_type.minus] = "-",
	[scanner.token_type.mul]   = "*",
	[scanner.token_type.div]   = "/",
};

---@param expression AST_Expression
---@return string
function ast.show_expression(expression)
	assert(expression.type, "(internal error: type is nil in ast.show_expression)");
	if expression.type == ast.type.expression_type.integer then
		return tostring(expression.value);
	elseif expression.type == ast.type.expression_type.string then
		return expression.value --[[@as string]];
	elseif expression.type == ast.type.expression_type.variable then
		return ast.show_variable(expression.value --[[@as AST_Variable]]);
	elseif expression.type == ast.type.expression_type.binary then
		local lhs = ast.show_expression(expression.value.lhs);
		local op = ast.show_binary_operator[expression.value.operator];
		local rhs = ast.show_expression(expression.value.rhs);
		return tostring(lhs) .. " " .. tostring(op) .. " " .. tostring(rhs);
	end
	return "(internal error: nil would have been returned in ast.show_expression for type " .. tostring(expression.type) .. ")";
end

---@param variable AST_Variable
---@return string
function ast.show_variable(variable)
	assert(variable.type, "(internal error: type is nil in ast.show_variable)");
	if variable.type == ast.type.variable_type.ident then
		return variable.value --[[@as string]];
	elseif variable.type == ast.type.variable_type.array then
		return "TODO: Array printing";
	end
	return "(internal error: nil would have been returned in ast.show_variable for type " .. tostring(variable.type) .. ")";
end

---@alias AST_Variable {type: variable_type, value: string|AST_ArrayAccess}
---@alias AST_Constant {name: string, type_expression: string, value: AST_Expression}
---@alias AST_Instance {name: string, class_name: string, assignments: table<string|table, AST_Expression>}
---@alias AST_ArrayAccess {array: AST_Variable, expression: AST_Expression}
---@alias AST_BinaryExpression {operator: token_type, lhs: AST_Expression, rhs: AST_Expression}
---@alias AST_Expression {type: expression_type, value: integer|string|AST_Variable|AST_BinaryExpression}



---@param cause string
---@param filepath string
---@param pos integer
function error.fatal(cause, filepath, pos)
	io.stderr:write("Fatal error in " .. scanner.format_source_location(filepath, pos) .. ": " .. cause .. "\n");
	os.exit(1);
end

---@param cause string
---@param filepath string
---@param pos integer
function error.parser_error(cause, filepath, pos)
	io.stderr:write("Parser error in " .. scanner.format_source_location(filepath, pos) .. ": " .. cause .. "\n");
end
